use-package (2.4.4-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 26 Jul 2024 11:28:14 +0900

use-package (2.4.4-1) unstable; urgency=medium

  * New upstream version 2.4.4
  * Refresh patches
  * Do not run tests from use-package-chords-tests.el
  * d/control: Declare Standards-Version 4.6.1 (no changes needed)
  * d/copyright: Update copyright information

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 22 Nov 2022 13:59:06 +0500

use-package (2.4.1-1) unstable; urgency=medium

  * New upstream version 2.4.1
  * Migrate to debhelper 13 without d/compat
  * d/control: Declare Standards-Version 4.5.0 (no changes needed)
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 14 Nov 2020 10:21:35 +0500

use-package (2.4-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Mon, 26 Aug 2019 14:32:00 -0300

use-package (2.4-2) unstable; urgency=medium

  * Add missing use-package-bind-key.el to elpa-use-package
  * d/control: Depend on elpa-diminish
  * d/control: Suggest other use-package packages

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 02 Jan 2019 10:43:20 +0500

use-package (2.4-1) unstable; urgency=medium

  * New upstream version 2.4
  * Add patch to clean documentation
  * Add patch to fix version numbers
  * Build separated packages as distributed by MELPA
  * Migrate to dh 11
  * Remove information on package split
  * d/control: Add Rules-Requires-Root: no
  * d/control: Add Testsuit declaration
  * d/control: Build-depend on install-info and texinfo
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org
  * d/control: Declare Standards-Version 4.3.0 (no changes needed)
  * d/control: Drop emacs25 for Enhances
  * d/control: Update Maintainer (to Debian Emacsen team)
  * d/copyright: Update copyright information (years bump, migration to GPL-3+)
  * d/rules: Build info documentation
  * d/rules: Do not skip tests
  * d/rules: Install upstream's changelog
  * d/rules: Skip upstream's make install

 -- Lev Lamberov <dogsleg@debian.org>  Mon, 31 Dec 2018 22:58:02 +0500

use-package (2.3+repack-2) unstable; urgency=medium

  * d/control: Fix Homepage URL (Closes: #882594)
  * d/control: Enhance emacs25, not emacs24
  * d/control: Do not depend on emacs
  * d/control: Declare Standards-Version 4.1.1 (no changes needed)
  * d/control: Drop Built-Using field
  * d/control: Drop Testsuit field
  * d/control: Adjust dependencies
  * d/rules: Do not pass --parallel to dh (compat is 10)

 -- Lev Lamberov <dogsleg@debian.org>  Fri, 24 Nov 2017 20:52:57 +0500

use-package (2.3+repack-1) unstable; urgency=medium

  * New upstream version 2.3+repack

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 27 Oct 2016 23:50:19 +0500

use-package (2.2+repack-1) unstable; urgency=low

  * Initial release (Closes: #841852)

 -- Lev Lamberov <dogsleg@debian.org>  Mon, 24 Oct 2016 00:55:47 +0500
